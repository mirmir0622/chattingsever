#pragma once

namespace chatting
{
	namespace db
	{
		enum class EUserRow
		{
			UserNumber,
			UserID,
			UserPassword,
			NickName,
			Profile,
			isOnline,
			Count
		};

		const char* USER_ROW[static_cast<int>(EUserRow::Count)] = { "UserNumber", "UserID", "UserPassword", "NickName", "Profile", "isOnline" };

		enum class ERoomRow
		{
			RoomNumber,
			RoomName,
			RoomMasterNumber,
			Count
		};

		const char* ROOM_ROW[static_cast<int>(ERoomRow::Count)] = { "RoomNumber", "RoomName", "RoomMasterNumber" };
	}
}