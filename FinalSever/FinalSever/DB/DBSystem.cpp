#include <iostream>
#include <cassert>
#include <string>

#include "EOrder.h"
#include "DBSystem.h"
#include "..//TinyXML-2/tinyxml2.h"
#include "..//MysqlFree.h"
#include "..//ERow.h"
#include "..//CXML.h"

#pragma comment (lib, "libmariadb.lib")

namespace chatting
{
	namespace db
	{
		DBSystem::DBSystem()
			: mExit(false)
		{
			const char* HOST = "localhost";
			const char* DB_ID = "chatMaster";
			const char* DB_PASS = "seoil";
			const char* DB_NAME = "chatting";
			const int32_t PORT = 3306;

			mysql_init(&mMysql);

			//Connect is Fail
			if (nullptr == mysql_real_connect(&mMysql, HOST, DB_ID, DB_PASS, DB_NAME, PORT, NULL, 0))
			{
				std::cout << mysql_error(&mMysql);
				assert(false && "DBSystem::DBSystem()::mysql_real_connect() is Error");
			}
		}

		DBSystem::~DBSystem()
		{
			mysql_close(&mMysql);
		}

		bool DBSystem::SendQuery(const char* s, std::vector<std::string*>& vector)
		{
			tinyxml2::XMLDocument xmlDoc;
			xmlDoc.Parse(s);
			return CheckQuery(xmlDoc.FirstChild()->NextSibling(), vector);
		}

		bool DBSystem::CheckQuery(tinyxml2::XMLNode* root, std::vector<std::string*>& vector)
		{
			tinyxml2::XMLNode* XMLorder = root->FirstChildElement("Order")->FirstChild();
			int32_t order = atoi(XMLorder->Value());

			bool check = false;
			switch (static_cast<EOrder>(order))
			{
			case (EOrder::Login):
			{
				check = ProessLogin(vector, root);
				break;
			}
			default:
				check = false;
				assert(false);
				break;
			}

			return check;
		}

		MYSQL_RES* DBSystem::SendSQL(const std::string& sql)
		{
			int check = mysql_real_query(&mMysql, sql.c_str(), static_cast<unsigned long>(sql.size()));

			if (check != 0)
			{
				assert(false);
				return nullptr;
			}

			return mysql_store_result(&mMysql);
		}

		bool DBSystem::ProessLogin(_Out_ std::vector<std::string*>& vector, tinyxml2::XMLNode* root)
		{
			tinyxml2::XMLNode* UserID = root->FirstChild()->NextSibling()->FirstChild();
			tinyxml2::XMLNode* UserPassword = root->LastChild()->LastChild();

			std::string sql = "Select * from User where UserID = \'";
			sql += UserID->Value();
			sql += "\' AND UserPassword = \'";
			sql += UserPassword->Value();
			sql += "\' AND isOnline = 0;";

			MysqlFree userRes(SendSQL(sql));
			MYSQL_ROW user = mysql_fetch_row(userRes.GetRes());
			{
				CXML xml;
				xml.NewNode("Response");
				xml.NewElement("Order", EOrder::Login);

				int32_t loginSucces = 0;
				if (nullptr != user)
				{
					xml.NewElement(USER_ROW[static_cast<int>(EUserRow::UserNumber)], user[static_cast<int>(EUserRow::UserNumber)]);
					loginSucces = 1;
				}
				
				xml.NewElement(USER_ROW[static_cast<int>(EUserRow::UserID)], user[static_cast<int>(EUserRow::UserID)]);
				xml.NewElement(USER_ROW[static_cast<int>(EUserRow::isOnline)], loginSucces);
				vector.push_back(new std::string(xml.Print()));

				if (nullptr == user)
				{
					return false;
				}
			}

			{
				sql = "update user set isOnline = 1 WHERE UserNumber = ";
				sql += user[static_cast<int>(EUserRow::UserNumber)];
				SendSQL(sql);

				CXML xml;
				xml.NewNode("Response");
				xml.NewElement("Order", EOrder::UserInfo);
				xml.NewElement(USER_ROW[static_cast<int>(EUserRow::UserNumber)], user[static_cast<int>(EUserRow::UserNumber)]);
				xml.NewElement(USER_ROW[static_cast<int>(EUserRow::NickName)], user[static_cast<int>(EUserRow::NickName)]);
				xml.NewElement(USER_ROW[static_cast<int>(EUserRow::Profile)], user[static_cast<int>(EUserRow::Profile)]);

				vector.push_back(new std::string(xml.Print()));
			}

			{
				sql = "select * from Room where RoomNumber IN (select RoomNumber from userList where userNumber = ";
				sql += user[static_cast<int>(EUserRow::UserNumber)];
				sql += ");";

				CXML xml;
				xml.NewNode("Response");
				xml.NewElement("Order", static_cast<int>(EOrder::RoomInfo));

				MysqlFree roomInfoRes(SendSQL(sql));
				MYSQL_ROW room;

				while ((room = mysql_fetch_row(roomInfoRes.GetRes())) != NULL)
				{
					xml.NewNode("Room");
					xml.NewElement(ROOM_ROW[static_cast<int>(ERoomRow::RoomNumber)], room[static_cast<int>(ERoomRow::RoomNumber)]);
					xml.NewElement(ROOM_ROW[static_cast<int>(ERoomRow::RoomName)], room[static_cast<int>(ERoomRow::RoomName)]);
					xml.NewElement(ROOM_ROW[static_cast<int>(ERoomRow::RoomMasterNumber)], room[static_cast<int>(ERoomRow::RoomMasterNumber)]);
				}

				vector.push_back(new std::string(xml.Print()));

				return true;
			}
		}
	}
}