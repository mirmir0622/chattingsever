#pragma once
#include <string>

namespace chatting
{
	namespace db
	{
		enum class EOrder
		{
			Login = 0,
			UserInfo = 3,
			RoomInfo = 5,
			NewUser,
			DeleteUser,
			Logout,
			NewRoom,
			DeleteRoom,
			BanishUser,
			AddUser,
			AddChat,
			Count
		};

		//const std::string ORDER[static_cast<int>(EOrder::Count)] = { "NewUser", "DeleteUser", "Login", "Logout", "NewRoom", "DeleteRoom", "BansihUser", "AddUser", "AddChat" };
	}
}