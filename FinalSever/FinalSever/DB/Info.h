#pragma once
#include <cstdint>
#include <string>

#include "EOrder.h"

namespace chatting
{
	namespace db
	{
		struct Info
		{
			EOrder Order;
		};

		struct LoginInfo : public Info
		{
			std::string UserNumber;
			std::string UserID;
			std::string UserPassword;
			std::string isOnline;
		};

		struct UserInfo : public Info
		{
			std::string UserNuber;
			std::string UserID;
			std::string NickName;
			std::string Profile;
			std::string isOnline;
		};
		
		struct RoomInfo : public Info
		{
			std::string RoomNumber;
			std::string RoomName;
			std::string RoomMasterNumber;
		};

		struct UserListInfo : public Info
		{
			std::string RoomNumber;
			std::string UserNumber;
		};

		struct FriendInfo : public Info
		{
			std::string UserNumber;
			std::string FriendNumber;
			std::string isAccept;
		};

		struct FileInfo : public Info
		{
			std::string RoomNumber;
			std::string UserNumber;
			std::string FileName;
			std::string Date;
		};

		struct ChattingInfo : public Info
		{
			std::string RoomNumber;
			std::string UserNumber;
			std::string Chat;
			std::string Date;
		};
	}
}