#pragma once
#include <string>
#include <queue>

#include "..//TinyXML-2/tinyxml2.h"
#include "..//include/mysql.h"
#include <vector>

namespace chatting
{
	namespace db
	{
		class DBSystem
		{
		public:
			DBSystem();
			~DBSystem();
			DBSystem(const DBSystem& other) = delete;
			DBSystem& operator=(const DBSystem& other) = delete;

			bool SendQuery(const char* s, std::vector<std::string*>& vector);

		private:
			bool CheckQuery(tinyxml2::XMLNode* root, std::vector<std::string*>& vector);
			MYSQL_RES* SendSQL(const std::string& sql);
			
		private:
			bool ProessLogin(_Out_ std::vector<std::string*>& vector, tinyxml2::XMLNode* root);

		private:
			bool mExit;
			MYSQL mMysql;
		};
	}
}