#include "core/Sever.h"
#include "DB/DBSystem.h"

using namespace chatting;

int main()
{
	core::Sever sever;

	sever.Run();

	return 0;
}