#pragma once
#include "include/mysql.h"
#include <memory>

namespace chatting
{
	namespace db
	{
		class MysqlFree
		{
		public:
			MysqlFree() = delete;
			~MysqlFree() = default;
			MysqlFree(const MysqlFree& other) = delete;
			MysqlFree& operator=(const MysqlFree& other) = delete;

			MysqlFree(MYSQL_RES* res)
				: mRes(std::make_unique<MYSQL_RES>(std::move(*res)))
			{
			}

			inline MYSQL_RES* GetRes() const
			{
				return mRes.get();
			}

		private:
			std::unique_ptr<MYSQL_RES> mRes;
		};
	}
}