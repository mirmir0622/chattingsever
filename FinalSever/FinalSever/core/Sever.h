#pragma once
#include <cstdint>
#include <memory>
#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")

//sql인젝션
//SSH보안프로토콜사용(빡시게)
//보안프로그램돌릴수있으면 돌려보기
namespace chatting
{
	namespace db
	{
		class DBSystem;
	}

	namespace core
	{
		//Windows SeverSystem;
		class Sever
		{
		public:
			Sever();
			~Sever();
			Sever(const Sever& other) = delete;
			Sever& operator=(const Sever& other) = delete;

			void Run(); //accept;
			void Exit();

		private:
			void Communicate(const SOCKET hclient, SOCKADDR_IN clientAddr); //Send/Recv;
			void Login(_In_ SOCKET hclient, _In_ SOCKADDR_IN clentAddr);

		public:
			static const int32_t MAX_CLIENT;
			const int32_t SUCCESS = 0;
			const int32_t MAX_LOOP = 10;

		private:
			const int32_t PORT = 7001;
			const int32_t PACKET_SIZE = 1024;
			bool mExit;
			SOCKET mListen;
			std::unique_ptr<db::DBSystem> mDB;
		};
	}
}