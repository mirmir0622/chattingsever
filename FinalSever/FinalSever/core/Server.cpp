#include <iostream>
#include <cassert>
#include <string>

#include "..//DB/DBSystem.h"
#include "Chatting.h"
#include "Sever.h"

namespace chatting
{
	namespace core
	{
		const int32_t Sever::MAX_CLIENT = 8;

		Sever::Sever()
			: mDB(std::make_unique<db::DBSystem>())
			, mExit(false)
		{
			WSADATA wasData;

			const int32_t errorCode = WSAStartup(MAKEWORD(2, 2), &wasData);
			if (errorCode != Sever::SUCCESS)
			{
				PrintError(errorCode, "Sever::Sever()::WSAStartup() is Error");
				assert(false);
			}

			mListen = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
			SOCKADDR_IN listenAddr = {};
			listenAddr.sin_family = AF_INET;
			listenAddr.sin_port = htons(PORT);
			listenAddr.sin_addr.s_addr = htonl(INADDR_ANY);

			if (Sever::SUCCESS != bind(mListen, reinterpret_cast<SOCKADDR*>(&listenAddr), sizeof(listenAddr)))
			{
				PrintError(WSAGetLastError(), "Sever::Sever()::bind() is Error");
				assert(false);
			}

			if (Sever::SUCCESS != listen(mListen, MAX_CLIENT))
			{
				PrintError(WSAGetLastError(), "Sever::Sever()::bind() is Error");
				assert(false);
			}
		}

		Sever::~Sever()
		{
			if (Sever::SUCCESS != WSACleanup())
			{
				PrintError(WSAGetLastError(), "Sever::~Sever()::WSACleanup() is Error");
				assert(false);
			}
		}

		void Sever::Exit()
		{
			mExit = true;
			//mDBSystem->Exit();
		}

		void Sever::Run()
		{
			std::cout << "�����۵�" << std::endl;

			while (true)
			{
				SOCKADDR_IN clientAddr = {};
				int32_t clientSize = sizeof(clientAddr);
				SOCKET hclient = accept(mListen, reinterpret_cast<sockaddr*>(&clientAddr), &clientSize);
				
				Login(hclient, clientAddr);

				//new Thread
				Communicate(hclient, clientAddr);
			}
		}


		void Sever::Communicate(const SOCKET hclient, SOCKADDR_IN clientAddr)
		{
			char* buffer = new char[PACKET_SIZE];
			int clientSize = sizeof(clientAddr);
			std::vector<std::string*> vector;
			while (true)
			{
				memset(buffer, 0, PACKET_SIZE);
				recvfrom(hclient, buffer, PACKET_SIZE, 0, reinterpret_cast<sockaddr*>(&clientAddr), &clientSize);

				mDB.get()->SendQuery(buffer, vector);

				sendto(hclient, buffer, static_cast<int32_t>(strlen(buffer)), 0, reinterpret_cast<sockaddr*>(&clientAddr), clientSize);
			}

			closesocket(hclient);
		}

		void Sever::Login(_In_ SOCKET hclient, _In_ SOCKADDR_IN clentAddr)
		{
			std::vector<std::string*> vector;
			vector.reserve(5);
			char* buffer = new char[PACKET_SIZE];
			memset(buffer, 0, PACKET_SIZE);
			int clintSize = sizeof(clentAddr);

			while (true)
			{
				recv(hclient, buffer, PACKET_SIZE, 0);

				bool check = mDB.get()->SendQuery(buffer, vector);

				send(hclient, vector[0]->c_str(), static_cast<int32_t>(vector[0]->size()), 0);
				if (false == check)
				{
					continue;
				}

				for (int index = 1; index < vector.size(); index++)
				{
					int count = sendto(hclient, vector[index]->c_str(), static_cast<int32_t>(vector[index]->size()), 0, reinterpret_cast<sockaddr*>(&clentAddr), clintSize);
				}
			}
			
		}
	} //namespace db
} //namespace chatting