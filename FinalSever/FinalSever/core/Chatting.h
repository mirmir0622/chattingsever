#pragma once
#include <iostream>
#include <cstdint>

namespace chatting
{
	inline void PrintError(int32_t errorNumber, const char* errorMessage)
	{
		std::cout << "ErrorNumber : errorNumber\n" << errorMessage;
	}
}