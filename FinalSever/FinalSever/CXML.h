#pragma once
#include "DB/EOrder.h"
#include "TinyXML-2/tinyxml2.h"

namespace chatting
{
	namespace db
	{
		class CXML
		{
		public:
			CXML()
				: mXmlDoc()
				, mElement(nullptr)
			{
				tinyxml2::XMLDeclaration* XMLDec = mXmlDoc.NewDeclaration();
				mXmlDoc.InsertFirstChild(XMLDec);
			}

			~CXML() = default;

			void NewNode(const char* name)
			{
				mXmlDoc.InsertEndChild(mXmlDoc.NewElement(name));
			}

			void NewElement(const char* name, const char* value)
			{
				mElement = mXmlDoc.NewElement("name");
				mElement->SetText(value);
				mXmlDoc.InsertEndChild(mElement);
			}

			void NewElement(const char* name, const int value)
			{
				mElement = mXmlDoc.NewElement("name");
				mElement->SetText(value);
				mXmlDoc.InsertEndChild(mElement);
			}

			void NewElement(const char* name, EOrder order)
			{
				mElement = mXmlDoc.NewElement("name");
				mElement->SetText(static_cast<int>(order));
				mXmlDoc.InsertEndChild(mElement);
			}

			inline const char* Print()
			{
				mXmlDoc.Print(&mPrint);
				return mPrint.CStr();
			}

		private:
			tinyxml2::XMLDocument mXmlDoc;
			tinyxml2::XMLElement* mElement;
			tinyxml2::XMLPrinter mPrint;
		};
	}
}